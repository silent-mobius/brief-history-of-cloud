
---

# Cloud

---

# The road to cloud

### Considerations

| Diffs  | On Premises | Cloud |
| ---    |       ---   |   --- |
| Scalability    |Pay more, get less. Can scale up, yet not always scale down, maintenance downtimes are major issues  | Allows to use for what ever you use and scale up/down according to your usage and gauge. Can be automated with schedular and software API. |
| Server storage | A lot of space is required, and not always accessible. Maintenance/Upgrades are a hassle. | Cloud providers maintain the storage thus not come into your consideration. Storage space is only limited by the zone your prefer to use and you pay as you go |
| Data security  | It may be argued whether the local storage with appropriate security policy provides with sense of security, yet perpetrators still know that the data is in your server room storage  | Object storage and virtual drives do have single access point in cloud, yet it is monitored and managed by your security team actual drives the data sits on is unknown|
| Data loss      | Chances of data loss in case of faulty drive are not high, yet it happens and not always there is a backup to use  | cloud provides robust data recovery measures, such as storage caching, to prevent your data loss  |
| Maintenance    | IT team that needs requirements and know-how to manage, update/upgrade, scale, secure and provide service to teams, loading costs| There is maintenances windows, yet the backups are in place to provide you with support while that is happening  |

---

# Closer Look on Cloud Computing

- Delivery of on demand computer resources over internet on `pay as you go` basis
- The delivery mostly can work in 2 major models
    - Deployment model
    - Service model

---

# Deployment model

<img src="../99_misc/.img/deploy-strategy.jpg" alt="Deployment Strategies" style="border-radius: 25px;float:right;width:450px;">

The cloud deployment model identifies the specific type of cloud environment based on ownership, scale, and access, as well as the cloud’s nature and purpose. The location of the servers you’re utilizing and who controls them are defined by a cloud deployment model. It specifies how your cloud infrastructure will look, what you can change, and whether you will be given services or will have to create everything yourself. Relationships between the infrastructure and your users are also defined by cloud deployment types. Different types of cloud computing deployment models are described below 

- Public Cloud
- Private Cloud
- Hybrid Cloud

---

# Deployment model

### Public Cloud

<img src="../99_misc/.img/public.png" alt="Public Cloud" style="border-radius: 25px;float:right;width:450px;">

- Services are stored off-site and accessed over the internet
- It can be used by general public
- All hardware and software supporting infrastructure is owned and managed by cloud provider
    - Example: AWS,GCP,Azure, DigitalOcean, Akamai
    - In cases where the project developed in Cloud Native, meaning that the service everyone are using, is dependent on cloud resources, structure and availability.
---

# Deployment model

### Private Cloud
<img src="../99_misc/.img/private.png" alt="Private Cloud" style="border-radius: 25px;float:right;width:450px;">

- The cloud infrastructure is used exclusively by single organization
- The organization may run its private cloud or out source it to a hosting company
- The services and infrastructure are maintained on a private network
    - Examples: AWS, GCP, Azure, OpenStack, VMware, IBM, FBI, CIA
    - AWS, GCP and Azure can provide isolation network named VPC that can be accessed only via VPN and Strict network policies with multi-layer authentication.

---

# Deployment model

### Hybrid Cloud

<img src="../99_misc/.img/hybrid.png" alt="Hybrid Cloud" style="border-radius: 25px;float:right;width:450px;">

- It consists the functionalities of both Public and Private clouds
    - Example Nasa:
        - For the project deployment, they use hybrid cloud computing deployment.
        - Code, Tests and development are done on local network
        - Artifacts/Binaries are stored on object storage locally
        - They are pushed on stage network for pre-prod testing and eventually pushed on to production cloud.

---

# Service Models

<img src="../99_misc/.img/ipafaas.png" alt="IPAF-AAS" style="border-radius: 25px;float:right;width:450px;">

- Infrastructure As A Service: IAAS
- Software As A Service: SAAS
- Platform As A Service: PAAS

> [!] Note: there are many other abbreviations, such as [BAAS, CAAS, DAAS, FAAS, DBAAS and so on](https://en.wikipedia.org/wiki/As_a_service), yet not all are relevant, much less interesting. In case you wish to know, just ask and I shall answer.

---

# Service Models

### IAAS

<img src="../99_misc/.img/onprem_vs_cloud.png" alt="onprem-vs-cloud" style="border-radius: 25px;float:right;width:450px;">

IAAS is a cloud computing service model by means of which computing resources are supplied by a cloud services provider. The IaaS vendor provides the storage, network, servers, and virtualization in this case, to emulating computer hardware. This service enables users to free themselves from maintaining an on-premises data center. Typically IaaS involves the use of a cloud orchestration technology like like OpenStack or OpenNebula. It provides the customer with high-level APIs used to dereference various low-level details of underlying network infrastructure like backup, data partitioning, scaling, security, physical computing resources, etc.

---
# Service Models

### PAAS

<img src="../99_misc/.img/onprem_vs_cloud.png" alt="onprem-vs-cloud" style="border-radius: 25px;float:right;width:450px;">

PaaS vendors offer a development environment to application developers. The provider typically develops toolkit and standards for development and channels for distribution and payment. In the PaaS models, cloud providers deliver a computing platform, typically including an operating system, programming-language execution environment, database, and the web server. Application developers develop and run their software on a cloud platform instead of directly buying and managing the underlying hardware and software layers.

---

# Service Models

### SAAS

<img src="../99_misc/.img/onprem_vs_cloud.png" alt="onprem-vs-cloud" style="border-radius: 25px;float:right;width:450px;">

With SaaS model, users gain access to application software and databases. Cloud providers manage the infrastructure and platforms that run the applications. SaaS is sometimes referred to as "on-demand software" and is usually priced on a pay-per-use basis or using a subscription fee. In the SaaS model, cloud providers install and operate application software in the cloud and cloud users access the software from cloud clients. Cloud users do not manage the cloud infrastructure and platform where the application runs. This eliminates the need to install and run the application on the cloud user's own computers, which simplifies maintenance and support.

---

# Cloud providers

As mentioned during the talk here are main cloud providers
<table style="float:right">
    <tr>
        <td>
            <img src="../99_misc/.img/aws.png" alt="aws" style="border-radius: 25px;float:left;width:100px;">
        </td>
    </tr>
    <tr>
        <td>
            <img src="../99_misc/.img/gcp.png" alt="gcp" style="border-radius: 25px;float:left;width:100px;">
        </td>
    </tr>
    <tr>
        <td>
            <img src="../99_misc/.img/azure.png" alt="azure" style="border-radius: 25px;float:left;width:100px;">
        </td>
    </tr>
    <tr>
        <td>
            <img src="../99_misc/.img/do.png" alt="digitalocean" style="border-radius: 25px;float:left;width:100px;">
        </td>
    <tr>
        <td>
            <img src="../99_misc/.img/linode.png" alt="Linode" style="border-radius: 25px;float:left;width:100px;">
        </td>
    </tr>
</table>
- AWS 
- GCP 
- Azure 
- Digital Ocean 
- Akamai Linode 


---

# Private cloud providers

<table style="float:right">
    <tr>
        <td>
            <img src="../99_misc/.img/ibm.png" alt="ibm" style="float:right;width:100px;">
        </td>
    </tr>
    <tr>
        <td>
            <img src="../99_misc/.img/openstack.png" alt="openstack" style="float:right;width:100px;">
        </td>
    </tr>
    <tr>
        <td>
            <img src="../99_misc/.img/vmware.png" alt="vmware" style="float:right;width:100px;">
        </td>
    </tr>
</table>

- IBM 
- OpenStack 
- VMware 
 

