# Brief History Of Cloud



<!--
 # Created By: Silent-Mobius Aka Alex M. Schapelle
# Purpose: being lazy on new course setup
# Copyright (C) 2023  Alex M. Schapelle

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 -->

<footer>Created By Alex M. Schapelle, VAioLabs.io</footer>

---

# About The Workshop ?

We'll try to answer several topics mainly focused on:

- What is cloud ?
- What lead to evolution of cloud ?
- Who needs cloud ?
- How cloud works ?
- How to manage cloud ?


---

# Who Is This Workshop For ?

- The name kind of mentions it:
    - System administrators who wish to learn basic Cloud usage.
    - SysOps who are moving to DevOps jobs..
- But it also can be useful for:
    - Junior DevOps who wish to gain minimal knowledge of IaC.
    - Junior software developers who have no knowledge of IaC.

---

# Course Prerequisite

- TCP/IP knowledge - MUST
- UNIX/Linux shell usage - MUST
- Mild knowledge of shell scripting
- Moderate understanding of version control (git/github/gitlab) - Recommended
- Low-headed familiarity with containers (docker)

---

# Course Topics

- [Start](../00_init/README.md)
- [History](../01_History/README.md)
- [Cloud](../02_cloud/README.md)


---

# About Me

<img src="../99_misc/.img/me.jpg" alt="drawing" style="border-radius: 25px;float:right;width:180px;">

- Over 12 years of IT industry Experience.
- Fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - Between each semester, I tried to take IT course at various places.
        -  A+.
        -  Cisco CCNA.
        -  RedHat RHCSA.
        -  LPIC1 and Shell scripting.
        -  Other stuff I've learned alone.

---

# About Me (cont.)

- Over 7 years of sysadmin:
    - Shell scripting fanatic
    - Python developer
    - JS admirer
    - Golang fallen
    - Rust fan
- 5 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user

---

# About Me (cont.)

You can find me on the internet in bunch of places:

- Linkedin: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)
- Gitlab: [Silent-Mobius](https://gitlab.com/silent-mobius)
- Github: [Zero-Pytagoras](https://github.com/zero-pytagoras)
- ASchapelle: [My Site](https://aschapelle.com)
- VaioLabs-IO: [My company site](https://vaiolabs.io)


---

# About You

Share some things about yourself:

- Name and surname
- Job description
- What type of education do you poses ? formal/informal/self-taught/university/cert-course
- Do you know any of those technologies below ? What level ?
    - Docker / Docker-Compose / K8s
    - Jenkins
    - Git / GitLab / Github / Gitea / Bitbucket
    - Bash/PowerShell Script
    - Python3 / Pytest / Pylint / Flask
    - Go / Gin / Echo
    - Ansible / Terraform
- Do you have any hobbies ?
- Do you pledge your alliance to [Emperor of Man kind](https://warhammer40k.fandom.com/wiki/Emperor_of_Mankind) ?

---

# Let's Talk About Brief History Of Cloud

In order for to talk about cloud and it's uses, we'll have separate out story telling into 3 main parts:

- Old ages
- Modern days
- The future
- And beyond

> `[!]` Note: all in term of computing and not actual history.

---

# What is cloud ?

<img src="../99_misc/.img/cloud.jpg" alt="What is Cloud ?" style="border-radius: 25px;float:right;width:400px;">

Cloud, also known as cloud computing, is the on-demand availability of computer system resources, especially data storage (cloud storage) and computing power, without direct active management by the user.

> `[*]` Source [wikipedia](https://en.wikipedia.org/wiki/Cloud_computing)

---

# Fin

Thank you for listening }:->

---

# Narrative Fallacy

Cloud computing has a rich history that extends back to the 1960s, with the initial concepts of time-sharing becoming popularized via remote job entry . Users submitting jobs to operators to run on mainframes, was predominantly used during this era. This was a time of exploration and experimentation with ways to make large-scale computing power available to more users through time-sharing, optimizing the infrastructure, platform, and applications, and increasing efficiency for end users. 
All that needs to be mentioned in some manner to have a complete picture to where we stand, thus :

- We, and mostly me, love stories, thus we tend to simplify things
- Simplifying things, sometimes creates illusion of ease and simplicity
- Please, take into your consideration a fact of me trying to be story teller first and instructor second.

