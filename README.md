# Brief History Of Cloud

Based on online resources:

- [Wikipedia's cloud computing](https://en.wikipedia.org/wiki/Cloud_computing)
- [Alex Mackey's A Brief History Of Cloud](https://www.youtube.com/watch?v=61ArSpwDRFE)
- [Simplilearn's Cloud Computing In 6 Minutes](https://www.youtube.com/watch?v=M988_fsOSWo)
- [Anton Weiss's Introduction to AWS cloud](https://otomato.io/otoblog-2/)
- [GeeksforGeeks on cloud deployment models](https://www.geeksforgeeks.org/cloud-deployment-models/)

