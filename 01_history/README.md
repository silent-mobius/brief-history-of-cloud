
---

# History

---


# Old ages

<img src="../99_misc/.img/batch.jpg" alt="IBM 1442" style="border-radius: 25px;float:right;width:300px;">

Who is the the father of computers and cloud?

In 1963, the state of that age computing was based mostly on batch processing, based on mostly but not only,IBM 1442 card/punch machine, which required an operator.

---

# Old ages

### Time Sharing

<img src="../99_misc/.img/backus.jpg" alt="John Backus" style="border-radius: 25px;float:right;width:300px;">

Suggested by [John Backus](https://en.wikipedia.org/wiki/John_Backus) in 1958, Organizations would install  computer on the premise and would **buy** time on the computers the same way that any house hold buys electricity and water from providers

- First demo demo of Time-Sharing was created at MIT, with Compatible Time-Sharing System (CTSS) in 1959
- First commercial system of Time Share system we got in 1963/4
with Dartmouth Time-Sharing System (DTSS)

---

# Old ages

### ARPANET

<img src="../99_misc/.img/arpanet.jpg" alt="ARPANET" style="border-radius: 25px;float:right;width:300px;">

- The Grandfather of modern internet and cloud computing, can be considered to be Advanced Research Projects Agency Network (ARPANET)
    - Initiated in 1963 and established by 1966 on the ideas of J. C. R. Licklider, Bob Taylor 
    - Foundations for first computers were based on several internal projects, besides them, project MAC for computation and mathematic theories
    - Building ARPANET was done by contractor [BBN](https://en.wikipedia.org/wiki/Raytheon_BBN), that have created "network" with 3 computers in California to a computer in Utah.
    - Reasons were based on scientific information share

---

# Old days

### First word

<img src="../99_misc/.img/arpanet_init.jpg" alt="First words" style="border-radius: 25px;float:right;width:300px;">

- In 1969 Charley Kline sends from UCLA to Stanford Research Institute First Word:
    - "Login"
    - Yet unfortunately it crashes and only `Lo` is delivered.
        - May be he intended to send `LOL` ?

---

# Old days

### Problem with ARPANET

<img src="../99_misc/.img/ncp.png" alt="NCProtocol" style="border-radius: 25px;float:right;width:300px;">

- ARPANET Knowledge shared by academics 
- Hard to make improvements
    - So they create group to handle it called Network Working Group (NWG)).
    - These guys create [RFC](https://en.wikipedia.org/wiki/Request_for_Comments) process still used today.
    - As part of RFC, [NCP] is create , which will make it easier to create communication between the computers

---

# Old days

### Compuserve AKA ISP

- Created to provide computer processing support to golden United Life Insurance
- They realize that they have a lot of unused computer hardware which they sell to other corporations.
- In 1978 began offering service to owners of personal computers to put under utilized assets to use at night.

---
 
# Old Days

### Around the world

Else-where between 1969 to 1970

- NPL networks in UK
- ALOHANET in Hawaii
- CYCLADES in Francxe which will be the basis for the internet

---

# Old days

### Era summary

- Reliable telephone line-based connections
- Modems to transmit data
- RFC process created and still used till this day
- Time period of expensive main-frames

---

# Modern Era

---

# Modern era

- Mainly 1970-1990
- 1972 ARPA shows first network demo
- 1978 personal computers are getting somewhat more reachable

---

# Modern era

### 1822 and NCP substitution by  TCP/IP and DNS

- ARPANET protocol 1822 and NCP were mostly designed for peer to peer communication, yet did not handle  inter-netting very well
- TCP/IP replaced NCP as ARPANETs principal protocol.
- DNS created in 1983

---

# Modern era

### Early internet

<img src="../99_misc/.img/timberneslee.jpg" alt="Tim Bernes Lee" style="border-radius: 25px;float:right;width:300px;">

- US based Tool and Die launched "The World"
    - They were the first Internet Service Providers in 1989
    - They were somewhat controversial and governments and universities threatened to block on grounds of internet was no for commercial use
- 1986 Australia established ISP's and AU top level domain
- Era of bulleting boards boards and dial up modems
    - AOL were leaders of dial-up and modems with digital modems by providing free disks in 1991

> `[!]` Yet it was not so easy to use ...

---

# Modern era

### Tim Berners Lee

<img src="../99_misc/.img/http.png" alt="http" style="border-radius: 25px;float:right;width:400px;">

- In 1989, whilst at CERN, Tim creates two proposals that will become WWW
    - Hypertext + TCP/IP + DNS = WWW
    - In 1990 NeXT computer prototype demo is created

---

# Era summary

- Reliable and fast communications infrastructure
- Essential protocol including TCP/IP, DNS,HTTP developer
- Development in processing power and computers are cheap enough for individuals to purchase (around $1200)
- Due tp inexpensive hardware and network equipment hosting is done in-house

---

# The Future

---

# The Future

### The big boom (part 1)

<img src="../99_misc/.img/mosaic.jpg" alt="Mosaic browser" style="border-radius: 25px;float:right;width:300px;">

<img src="../99_misc/.img/bill_nt_box.jpg" alt="Bill Gates pushes for NT boxes" style="border-radius: 25px;float:right;width:300px;">

<img src="../99_misc/.img/freebsd.png" alt="FreeBSD" style="border-radius: 25px;float:right;width:300px;">

- The mosaic browser is created in 1993
- Bill Gates starts to push towards NT boxes as an http server in 1995 
- BSD is working towards best internet server as an integrated


---

# The Future

### The big boom (part 2)

<img src="../99_misc/.img/dotcom.png" alt="Dot Com Boom" style="border-radius: 25px;float:right;width:400px;">

- Dot Com Boom
    - Easy to raise venture capital due to low interest rates and lowered capital gains tax
    - Telecommunications makes massive infra investment
    - Netscape is evaluated with price tag of 2.9 Billion
- Jeff Bezos starts selling books online in 1994
- George Favaloro, Compaq business director, coins the term [Cloud Computing](http://www.technologyreview.com/news/425970/who-coined-cloud-computing/) in 1996
- Marc Benioff founds, arguably one of the first SaaS business named Salesforce

---

# The Future

### The big boom (part 3)

<img src="../99_misc/.img/dotcom.png" alt="Dot Com Boom" style="border-radius: 25px;float:right;width:400px;">

- Huge increase with mobile usage from 1997
- Hosting era from 2000-2001
    - Era of large data centers
    - Need for fast provisioning of equipment
- Nasdaq deals burst throughout the roof creating money from everything that has `tech` in its name
    - Creating Dot Com bubble
---

# The Future

### The Bubble Kaboom

- By the end of 2001 the Dot Com bubble has crashed
    - By the end of the bubble around 5-7 billions were lost
    - What happened ?
        - Over valued companies and use of metrics that ignored cash flow
        - Changes in interest rates
        - Accounting scandals such as [World.com](https://en.wikipedia.org/wiki/WorldCom_scandal) and [Enron](https://en.wikipedia.org/wiki/Enron_scandal)
        - 9/11 events
    - Outcome:
        - Less investments
        - More cautions investors
        - Layoffs of tech staff
        - Focus on bottom line

---

# The Future

### Amazon

- Amazon pushing merchant.com
- Company growing quick and IT isn't keeping up
- starts splitting codebase into independent services

---

# The Future

### Amazon

- Myth
    - AWS came out of unused infrastructure
- Reality
    - Chris Pinkham and Benjamin Black, thinking about services as a kind of operating system for the internet and proposing company could generate revenue from new infrastructure in 2003
- 2004 Amazon starts working on AWS and Bezos rants about [enablement of small businesses from their infrastructure](https://aws.amazon.com/blogs/aws/welcome)
    - Personally, I agree that AWS allows soft beginning for small developer/group
        - Lets keep in mind that without AWS, a lot of companies would not exists today.
    - Yet the embrace of cloud comes with less difficulties

---

# The Future 

### Launch

- 2006 Amazon  launches `Amazon Web Service`
- The core services they started with:
    - Simple Queue Service (SQS)
    - Simple Storage Service (S3)
    - Elastic Compute Cloud (EC2)
- By 2009 S3 and EC2 launched in Europe
- By 2010 Amazon fully runs on AWS
- By 2020 AWS has more then 20 gro-graphical regions and growing

---

# The Future

### Nemesis and Other kids on the block

- 2007 Microsoft RedDog Project (Early Azure project)
    - Released publicly by 2010, mostly thanks to [Satya Nadela](https://en.wikipedia.org/wiki/Satya_Nadella) and Linux
- 2008 starts with Google Cloud Platform (GCP)
    - Released publicly around 2011
- 2009 joined the race by Alibaba cloud
- 2010 NASA and Rackspace announces OpenStack initiative.

---

# The Future

### Breakthroughs

- 2013 Docker emerges with containers
- 2015 Kubernetes starts to manage containers on multi nodes
- 2018 AWS provides docker and k8s provides services
- 2019 Rancher labs provide k8s on the edge named k3s

---

# The Future 

### 2020+ Trends

- Infra as a code
- Multi-region infrastructure
- Multi-Cloud redundancy
- Increase use of abstractions, less marinating, more deploying
- Competitors embracing each others tech
- Need for better enterprise cost management
- More legislation and increased government interference

---

# Era summary ?

- Rapid changes in tech
- Rapid economy changes
- Rapid requirements changes
- There is no end

---

# Beyond Future

---

# Beyond Future

### Why do we need to know all this ?

- "Those who cannot remember the past are doomed to repeat it." George Santayana, Writer and Philosopher
- "We spend a great deal of time studying history, which let's face it is mostly history of stupidity" Stephan Hawking

---

# Beyond Future

### The futures cone

<img src="../99_misc/.img/futures-cone.jpg" alt="future cone" style="border-radius: 25px;float:right;width:600px;">

The futures cone, provides illustration of the variety of possibilities in futures that emanate from the current moment in time. It is mostly applied to strategy, analysis of the range of futures, AKA prediction.

As you gaze into the future, there are a great many things that could potentially happen emanating from the current moment in time. The further into the future your gaze extends, the broader the range of potential futures that come into view. The Futures Cone visualizes this expanding range of potential futures as a set of nested cones, with each cone representing a different degree of future likelihood

---

# Beyond Future

- Data wars
- AI revolution
- AR/VR comeback 
